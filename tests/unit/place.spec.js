import Place from "../../src/models/place"
import { expect } from 'chai'
describe("Place", () => {
    it ("have default values when created", ()=>{
        let p = new Place();
        expect(p).to.be.a('object');
        expect(p.id).eq(0);
        expect(p.name).eq("");
        expect(p.lat).eq(0);
        expect(p.lng).eq(0);
        expect(p.type).eq("");
    });

    it ('can be created via factory constructor', ()=>{
        let p = Place.createOne(123, "Bugis+", "Shopping Mall", 12, 34 );
        expect(p).to.be.a('object');
        expect(p.id).eq(123);
        expect(p.name).eq("Bugis+");
        expect(p.lat).eq(12);
        expect(p.lng).eq(34);
        expect(p.type).eq("Shopping Mall");
    })
});