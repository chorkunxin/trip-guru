import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import MockFindPlaces from '../../src/services/MockFindPlaces'
import MockFindCities from '../../src/services/MockFindCities'
import {PlaceFacade} from '../../src/facade/Places'
import Autocomplete from '../../src/components/Autocomplete'
import flushPromises from 'flush-promises'

describe("Autocomplete.vue", () => {
   it("should show search results", ()=>{
       const wrapper = shallowMount(Autocomplete, {
           propsData: {}
       });

       wrapper.setData({
          isOpen:true,
          results: [
              {
                  id: 1,
                  name: "Singapore"
              },
              {
                  id: 2,
                  name: "Malaysia"
              }
          ]
       });

       wrapper.vm.$nextTick(()=>{
           expect(wrapper.findAll(".each-result").length).eq(2);
       });
   });
   it("should show search results on typing", async ()=>{

       PlaceFacade.findPlaceServices = new MockFindPlaces();
       PlaceFacade.findCityServices = new MockFindCities();
       const wrapper = shallowMount(Autocomplete, {
           propsData: {
               datasource: (text) =>{
                   return PlaceFacade.loadPossibleCities({params:text});
               }
           }
       });

       let textField = wrapper.find(".input");
       textField.element.value = "Singapore";
       textField.trigger("input");
       await flushPromises();
       wrapper.vm.$nextTick(()=>{
           expect(wrapper.findAll(".each-result").length).eq(2);
           expect(wrapper.text()).to.include("Singapore");
           expect(wrapper.text()).to.include("Malaysia");
       });
   });

    it("should be able to select an item", async() => {
        PlaceFacade.findPlaceServices = new MockFindPlaces();
        PlaceFacade.findCityServices = new MockFindCities();
        const wrapper = shallowMount(Autocomplete, {
            propsData: {
                datasource: (text) =>{
                    return PlaceFacade.loadPossibleCities({params:text});
                }
            }
        });

        let textField = wrapper.find(".input");
        textField.element.value = "Singapore";
        textField.trigger("input");
        await flushPromises();

        wrapper.vm.$nextTick(()=>{
            let firstOption = wrapper.find('.each-result');
            firstOption.trigger('click');

            expect(wrapper.vm.selectedItem.id).eq(1);
        });

    })
});