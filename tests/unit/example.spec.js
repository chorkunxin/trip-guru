import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'
import MockFindPlaces from '../../src/services/MockFindPlaces'
import Place from "../../src/models/place";
import {PlaceFacade} from "../../src/facade/Places"

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    });
    expect(wrapper.text()).to.include(msg)
  });

    it("has access to Places facade", ()=>{
        const wrapper = shallowMount(HelloWorld);
        expect(wrapper.vm.places).is.a('object');
        expect(wrapper.vm.places.getPlaces()).to.be.a("array");
    });

    it("has can render from Places facade", ()=>{
        const wrapper = shallowMount(HelloWorld);
        let mockFindPlaces = new MockFindPlaces();
        mockFindPlaces.getPlacesBy().then(function(places){
            wrapper.vm.places.setPlaces(places);
            expect(wrapper.findAll(".place-item").length).eq(places.length);
            places.forEach( (place)=> {
                expect(wrapper.text()).to.include(place.name);
            });
        });


    });

    it("can update when there are new Places", ()=>{
        const wrapper = shallowMount(HelloWorld);
        let mockFindPlaces = new MockFindPlaces();

        mockFindPlaces.getPlacesBy().then(function(places){
            wrapper.vm.places.setPlaces(places);
            PlaceFacade.places.push(Place.createOne(4, "Botanic Garden", "Park", 23, 34));
            wrapper.vm.$nextTick(()=>{
                let places = PlaceFacade.getPlaces();
                expect(wrapper.findAll(".place-item").length).eq(places.length);
                places.forEach( (place)=> {
                    expect(wrapper.text()).to.include(place.name);
                });
            });

        });
    });
});
