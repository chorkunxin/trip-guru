import { expect } from 'chai'
import config from '../../src/config'

describe ('config', ()=>{
   it ('can fetch api key from config', ()=>{
       expect(config.API_KEY).to.be.a('string');
   });
});