# Trip Guru
Trip Guru is a Google Map enabled SAP that allows you to select
a city, and then what are you looking for in that city. Once you have do so, you are able to look at the establishments
that of interest to you.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

##Techniques/Technology Used
* Data binding
* Nested components
* Vue-router
* Axios
* Separating logic from views
* Bulma CSS for responsive design
* Promises
* Events handling

