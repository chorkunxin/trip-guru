export default class Place {
    constructor() {
        this.name = "";
        this.type = "";
        this.id = 0 ;
        this.lat = 0;
        this.lng = 0;
        this.description = "";
        this.address = "";
        this.image = "";
        this.reviews = [];
    }

    static  createOne(id, name, type, lat, lng, description, address, image, reviews=[], ratings=0, opening_hours={})
    {
        let p = new Place();
        p.id = id;
        p.name = name;
        p.type = type;
        p.lat = lat;
        p.lng = lng;
        p.description = description;
        p.address = address;
        p.image =image;
        p.reviews = reviews;
        p.ratings = ratings;
        p.opening_hours = opening_hours;
        return p;
    }
}