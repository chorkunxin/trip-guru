import MockFindPlaces from "../services/MockFindPlaces";
import MockFindCities from "../services/MockFindCities";
import FindCities from "../services/FindCities";
import config from "../config";
import FindPlaces from "../services/FindPlaces";
import GetPlaceDetails from "../services/GetPlaceDetails";

class Places {
    constructor(findPlaceServices=null, findCityServices=null, getPlaceDetails=null) {

        this.findPlaceServices = findPlaceServices;
        this.findCityServices = findCityServices;
        this.getPlaceDetails = getPlaceDetails;
        this.places = [];
        this.cities = [];
        this.selected_city = null;
    }

    loadPlaces(params={}) {
       if (this.findPlaceServices) {
           return this.findPlaceServices.getPlacesBy(params).then((r)=>{
               this.places = r;
               return r;
           });
       } else {
           throw "No FindPlaceServices set";
       }

    }
    loadPossibleCities(text) {
        if (this.findCityServices) {
           return this.findCityServices.getCitiesBy(text).then( (r) => {
                this.cities = r;
                return r;
            })
        } else {
            throw "No FindCityServices set";
        }
    }
    loadPlaceDetails(placeId) {
        if (this.getPlaceDetails) {
            return this.getPlaceDetails.getDetailsBy(placeId);
        } else {
            throw "No GetPlaceDetails set";
        }
    }
    getPlaces() {
        return this.places;
    }
    setPlaces(places) {
        this.places = places;
    }
}


let PlaceFacade = new Places(new FindPlaces(config.API_KEY), new FindCities(config.API_KEY), new GetPlaceDetails(config.API_KEY));

export {PlaceFacade};