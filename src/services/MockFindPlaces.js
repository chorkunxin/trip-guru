import IFindPlaceService from "./IFindPlaceService";
import Place from "../models/place";

export default class MockFindPlaces extends IFindPlaceService {
    getPlacesBy(params) {
        return new Promise((resolve) => {
          resolve([
              Place.createOne(1, "Bugis+", "Shopping", 12, 34, "A shopping centre in town"),
              Place.createOne(2, "Northpoint Shopping Centre", "Shopping", 56, 78, "A shopping centre in the north"),
              Place.createOne(3, "Sembawang Shopping Centre", "Shopping", 90, 112, "An out of the way shopping centre in the north")
          ]);
      });
    }
}