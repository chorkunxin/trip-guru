import IFindPlaceService from "./IFindPlaceService";
import GoogleMapsLoader from "google-maps";
import Place from "../models/place";


export default class FindPlaces extends IFindPlaceService
{

    constructor(api_key) {
        super();
        this.api_key = api_key;
        this.placesService = null;
        this.google = null;

        GoogleMapsLoader.KEY = api_key;
        GoogleMapsLoader.LIBRARIES=['places'];
        GoogleMapsLoader.load((google) => {
            this.google = google;
            this.placesService =  new google.maps.places.PlacesService(document.createElement('div'));

        })
    }
    getPlacesBy(params) {
        let request ={
            query: params.categories + ' in ' +  params.city,
            type: params.categories,

        };
        return new Promise( (resolve,reject) => {

            this.placesService.textSearch(request, (results, status)=>{
                if (status === this.google.maps.places.PlacesServiceStatus.OK) {
                    resolve(results)
                } else {
                    reject(status);
                }
            })
        }).then((results)=>{
            return results.map((r)=>{
                return Place.createOne(r.place_id, r.name, r.icon,
                    r.geometry.location.lat(), r.geometry.location.lng(), "", r.formatted_address, r.photos[0].getUrl({
                        maxWidth: 120,
                        maxHeight:120
                    }) )
            })
        })

    }
}