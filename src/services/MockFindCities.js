import Place from "../models/place";

export default class MockFindCities {
    getCitiesBy(search_terms) {
        return new Promise((resolve) => {
            resolve([
                {id: 1, name: "Singapore"},
                {id: 2, name: "Malaysia"}
            ]);
        });
    }
}