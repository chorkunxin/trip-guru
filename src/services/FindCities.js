import IFindCities from "./IFindCities";
import axios from "axios";
import GoogleMapsLoader from "google-maps";

export default class FindCities extends IFindCities
{
    constructor(api_key) {
        super();
        this.api_key = api_key;
        this.auotcompleteService = null;
        this.google = null;
        console.log("api_key =" + api_key);
        GoogleMapsLoader.KEY = api_key;
        GoogleMapsLoader.LIBRARIES=['places']
        GoogleMapsLoader.load((google) => {
            this.google = google;
            this.autocompleteService =  new google.maps.places.AutocompleteService();

        })
    }

    getCitiesBy(search_terms) {
        let request = {
            input: search_terms,
            sensor:"false",
            types:"(cities)",
            key: this.api_key
        };

        return new Promise( (resolve,reject) => {

            this.autocompleteService.getQueryPredictions(request, (predictions, status)=>{
                if (status === this.google.maps.places.PlacesServiceStatus.OK) {
                    resolve(predictions)
                } else {
                    reject(status);
                }
            })
        }).then((predictions)=>{
            return predictions.map((p)=>{
                return {
                    id : p.id,
                    name : p.description
                }
            })
        })

    }
}