import IGetPlaceDetails from "./IGetPlaceDetails";
import GoogleMapsLoader from "google-maps";
import Place from "../models/place";

export default class GetPlaceDetails extends IGetPlaceDetails
{
    constructor(api_key)
    {
        super();
        this.api_key = api_key;
        GoogleMapsLoader.KEY = api_key;
        GoogleMapsLoader.LIBRARIES=['places'];

    }

    getDetailsBy(id) {
        const request = {
            placeId: id,
            fields: ['name', 'rating', 'formatted_address', 'photo', 'rating', 'review',
                'geometry', 'id', 'price_level', 'opening_hours']
        };

        // The first promise is to ensure that the google place API has been intiialized
        return new Promise((resolve,reject)=>{
            GoogleMapsLoader.load((google) => {
                this.google = google;
                this.placesService =  new google.maps.places.PlacesService(document.createElement('div'));
                console.log("place services created");
                resolve();
            })
        })
        .then(()=>{
            return new Promise( (resolve,reject) => {
                console.log("Getting details");
                this.placesService.getDetails(request, (place, status)=>{
                    if (status === this.google.maps.places.PlacesServiceStatus.OK) {
                        resolve(place)
                    } else {
                        reject(status);
                    }
                });
            })
        })
        .then((place)=>{
            console.log(place);
            return Place.createOne(
                place.id,
                place.name,
                place.types,
                place.geometry.location.lat(),
                place.geometry.location.lng(),
                "",
                place.formatted_address,
                place.photos.map(function(p){
                    return p.getUrl({maxWidth:240, maxHeight:240})
                }),
                place.reviews,
                place.rating,
                place.opening_hours
            )
        });

    }

}