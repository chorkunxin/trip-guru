export default {
    API_KEY: "AIzaSyBXP6HZ-8q5OMP1VR7H33UU30hmDgUYl6E",
    categoryToGooglePlaceMapping: {
        'food': 'restaurant',
        'drink': 'bar',
        'rest' : 'lodging',
        'shop' : 'shopping malls'
    },
    possibleCategories: {
        food: "Food",
        shop : "Shopping",
        rest: "Rest",
        drink: "Drink"
    }
}